.. index::
   pair: sphinx ; FAQ
   pair: Projets sphinx; ray
   pair: Projets sphinx; geopandas
   pair: confval; html_show_sourcelink
   pair: confval; language
   pair: index; page
   triple: literalinclude; start-after ; start-at
   triple: literalinclude; end-before; end-at
   ! literalinclude
   ! FAQ
   ! index

.. _faq:

================================================================================
|sphinx| **FAQ sphinx**
================================================================================


Comment enlever le 'Show source' à droite ?
================================================

Il faut initialiser la variable :confval:`html_show_sourcelink` du
fichier **conf.py** à False.

.. code-block:: python

   html_show_sourcelink = True


.. _faq_liste_themes:

Où peut-on trouver la liste des thèmes sphinx ?
===================================================

- http://sphinx-doc.org/theming.html


On a des listes ici:

- https://sphinx-themes.org/#themes
- https://awesomesphinx.useblocks.com/categories/themes.html

.. _faq_themes_employes:

Quels sont les thèmes les plus employés ?
============================================

Cela évolue bien sûr au cours du temps ; actuellement (2023-06-02) les thèmes
les plus employés sont:

- le thème `furo <https://pradyunsg.me/furo/>`_ pour les petits et moyens projets

  - `weblate <https://docs.weblate.org/en/latest/index.html>`_ utilise `ce thème <https://github.com/WeblateOrg/weblate/blob/main/docs/conf.py>`_

- le thème `pydata-sphinx-theme <https://pydata-sphinx-theme.readthedocs.io/en/stable/index.html>`_
  pour les gros projets scientifiques

  Voir `quelques projets employant ce thème <https://pydata-sphinx-theme.readthedocs.io/en/latest/examples/gallery.html>`_

- le thème :ref:`sphinx-material <sphinx_material>` come ce site de
  démonstration

- le `thème sphinx-rtd-theme <https://sphinx-rtd-theme.readthedocs.io/en/latest/>`_
- le `thème sphinx-book-theme <https://sphinx-book-theme.readthedocs.io/en/stable/>`_
  pour les gros projets comme `ray <https://docs.ray.io/>`_ par exemple

- le `récent thème immaterial <sphinx-immaterial/>`_
  Exemple : le `site recensant les extensions sphinx <https://awesomesphinx.useblocks.com/>`_

.. _faq_theme_site:

Quel est le thème employé sur ce site ?
=========================================

Le thème employé est :ref:`sphinx_material <sphinx_material>`


Comment créer la page d'index ?
==================================

La page d'index est créée au moyen des directives sphinx suivantes:

- index
- glossary


index
------

La directive index s'emploie de différentes manières

avec **!**
+++++++++++++

::

    .. index::
       ! Rubrique principale


Avec **pair:**
+++++++++++++++++

::

    .. index::
       pair: Classe ; ClasseExemple

glossary
------------

Cette directive est très puissante puisque tous les termes concernés
par glossary se retrouvent dans la page d'index.


Voir la :ref:`page d'index de ce projet <genindex>`.


Quels sont les documentations sphinx qui peuvent servir d'exemples ?
=======================================================================

Ces 3 sites recensent les meilleures documentations:

- https://github.com/readthedocs-examples/awesome-read-the-docs
- https://awesomesphinx.useblocks.com/
- https://github.com/yoloseem/awesome-sphinxdoc
- https://www.sphinx-doc.org/en/master/examples.html (le site officiel)


On peut citer les projets:

- `ray <https://docs.ray.io/>`_ (https://docs.ray.io/, récemment (2023-05-31) à la liste
  https://github.com/readthedocs-examples/awesome-read-the-docs#sphinx-projects

  Le `thème utilisé est sphinx-book-theme <https://github.com/ray-project/ray/blob/releases/2.4.0/doc/source/conf.py>`_

- `vsketch <https://vsketch.readthedocs.io/en/latest/autoapi/vsketch/index.html>`_
  pour l'utilisation de sphinx-auto-api

  - voir :ref:`tuto_sphinx:sphinx_antoine_beyeler_autoapi`
  - le thème `utilisé est furo <https://github.com/abey79/vsketch/blob/master/docs/conf.py>`_

- `geopandas <https://geopandas.org/en/stable/>`_

  Le thème utilisé est le `thème pydata-sphinx-theme <https://github.com/geopandas/geopandas/blob/main/doc/source/conf.py>`_


Où peut-on trouver la liste des extensions shinx ?
=======================================================

Voici les sites qui recensent les extensions sphinx:

- https://github.com/sphinx-contrib


Quel est l'intérêt de literalinclude ?
=========================================

- :ref:`tuto_formats:literal_include`

Si on inclut les sources d'un programme, le code est toujours correct.

En particulier, il est très intéressant d'employer :start-after et :end-before:


::

    .. literalinclude:: ../conf.py
       :language: bash
       :start-after: __begin_intersphinx_mapping__
       :end-before: __end_intersphinx_mapping__


Dans ce cas on cible le paramètrage du **tableau intersphinx_mapping[]**

.. literalinclude:: ../conf.py
   :language: bash
   :start-after: __begin_intersphinx_mapping__
   :end-before: __end_intersphinx_mapping__


.. important:: Il existe aussi les options :start-at et end-at

Voir :ref:`tuto_formats:start_at_end_at`


.. _faq_substitution:

|sphinx|  Comment paramétrer une substitution ?
=================================================

- https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html#substitutions

Pour insérer cet avatar |sphinx| aller dans le fichier conf.py et
ajouter cette ligne::

.. |sphinx| image:: /images/sphinx_avatar.png


à rst_prolog::

    rst_prolog = """
    .. |sphinx| image:: /images/sphinx_avatar.png
    .. |myst| image:: /images/myst_avatar.png
    .. |id3| image:: /images/id3_avatar.png
    """


Est-ce que la langue est paramètrable ?
=========================================

Au lieu de "Show source" j'ai le bouton "Monter le code source".

Cela signifie que la variable :confval:`language` est initialisée à **"fr"**.
