

# sidenotes

Sidenotes are numbered, and behave like footnotes, except they live in
the margin and don’t force the reader to jump their eye to the bottom of the page.

For example, here is a sidenote[^ex].
On narrow screens, sidenotes are hidden until a user clicks the number.
If you're on a mobile device, try clicking the sidenote number above.

[^ex]: Here's my sidenote text!

    On narrow screens, this text won't show up unless you click the superscript number!


# marginnotes

Marginnotes are not numbered, but behave the same way as sidenotes.
On mobile devices you'll see a symbol that will show the marginnote when clicked[^exmn].
For example, there's a marginnote in the previous sentence, and you should see a symbol show to display it on mobile screens.

[^exmn]: {-} This is a margin note. Notice there isn’t a number preceding the note.


Non utilisable avec le thème sphinx-material.
