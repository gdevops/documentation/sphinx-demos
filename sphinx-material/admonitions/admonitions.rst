.. index::
   pair: sphinx-material; admonitions
   pair: admonition; topic
   pair: admonition; admonition
   pair: admonition; attention
   pair: admonition; caution
   pair: admonition; danger
   pair: admonition; error
   pair: admonition; hint
   pair: admonition; important
   pair: admonition; note
   pair: admonition; seealso
   pair: admonition; tip
   pair: admonition; todo
   pair: admonition; warning
   pair: admonition; versionadded
   pair: admonition; versionchanged
   pair: admonition; deprecated



.. _admonitions:

===========
Admonitions
===========

Sphinx provides several different types of admonitions.

``topic``
=========

::

    .. topic:: This is a topic.

       This is what admonitions are a special case of, according to the docutils
       documentation.


.. topic:: This is a topic.

   This is what admonitions are a special case of, according to the docutils
   documentation.

``admonition``
==============

::

    .. admonition:: The one with the custom titles

       It's got a certain charm to it.


.. admonition:: The one with the custom titles

   It's got a certain charm to it.

``attention``
=============

::

    .. attention::

       Climate change is real.


.. attention::

   Climate change is real.

``caution``
===========

::

    .. caution::

       Cliff ahead: Don't drive off it.


.. caution::

   Cliff ahead: Don't drive off it.

``danger``
==========

::

    .. danger::

       Mad scientist at work!


.. danger::

   Mad scientist at work!

``error``
=========

::

    .. error::

       Does not compute.


.. error::

   Does not compute.

``hint``
========

::

    .. hint::

       Insulators insulate, until they are subject to ______ voltage.


.. hint::

   Insulators insulate, until they are subject to ______ voltage.

``important``
=============

::

    .. important::

       Tech is not neutral, nor is it apolitical.


.. important::

   Tech is not neutral, nor is it apolitical.

``note``
========

::

    .. note::

       This is a note.

.. note::

   This is a note.

``seealso``
===========

::

    .. seealso::

       Other relevant information.

.. seealso::

   Other relevant information.

``tip``
=======

::

    .. tip::

       25% if the service is good.


.. tip::

   25% if the service is good.

``todo``
========

::

    .. todo::

       This needs the ``sphinx.ext.todo`` extension.


.. todo::

   This needs the ``sphinx.ext.todo`` extension.

``warning``
===========

::

    .. warning::

       Reader discretion is strongly advised.


.. warning::

   Reader discretion is strongly advised.

``versionadded``
================

::

    .. versionadded:: v0.1.1

       Here's a version added message.


.. versionadded:: v0.1.1

   Here's a version added message.

``versionchanged``
==================

::

    .. versionchanged:: v0.1.1

       Here's a version changed message.


.. versionchanged:: v0.1.1

   Here's a version changed message.

``deprecated``
==============

::

    .. deprecated:: v0.1.1

       Here's a deprecation message.


.. deprecated:: v0.1.1

   Here's a deprecation message.
