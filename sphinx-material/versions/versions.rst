.. index::
   pair: sphinx-material; Versions

.. _sphinx_material_versions:

==============================================================================================
Versions
==============================================================================================

- https://github.com/bashtage/sphinx-material/releases


.. figure:: images/github_contributors.png
   :align: center

   https://github.com/bashtage/sphinx-material/graphs/contributors


.. toctree::
   :maxdepth: 3

   0.0.35/0.0.35
