.. index::
   ! reStructuredText

.. _sphinx_material_examples:

==============================================================================================
Examples
==============================================================================================

- https://github.com/bashtage/sphinx-material/
- https://sphinx-themes.org/#themes/
- :confval:`html_theme`


de Kevin Sheppard
===================

- https://bashtage.github.io/sphinx-material/
- https://www.statsmodels.org/stable/index.html
- https://bashtage.github.io/arch/index.html


Autres documentations sur sphinx et reStructuredText
==========================================================

- https://informatique.gitlab.srv.int.id3.eu/demos-sphinx/meta-demo/
- https://sphinx-extensions.readthedocs.io/en/latest/index.html
- :ref:`tuto_sphinx:tuto_sphinx`
- :ref:`tuto_formats:rest_sphinx_doc`
