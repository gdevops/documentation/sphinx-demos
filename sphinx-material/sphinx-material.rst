.. index::
   pair: sphinx-material; Theme
   pair: confval; html_theme

.. _sphinx_material:

==============================================================================================
**Theme sphinx-material** (A material-based, responsive theme inspired by mkdocs-material)
==============================================================================================

- https://github.com/bashtage/sphinx-material/
- https://sphinx-themes.org/#themes/
- :confval:`html_theme`


Installation
===============

::

    poetry add sphinx-material
    make update



Examples
================================================================

.. toctree::
   :maxdepth: 3

   examples/examples


Admonitions
==============

.. toctree::
   :maxdepth: 3

   admonitions/admonitions
   sidenotes/sidenotes


Versions
==============

.. toctree::
   :maxdepth: 3

   versions/versions
