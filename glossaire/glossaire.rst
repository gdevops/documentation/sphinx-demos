.. index::
   ! Glossaire

.. _glossaire:

================================================================================
**Glossaire** (tiré de face SDK)
================================================================================

- https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html#glossary-directive
- https://www.sphinx-doc.org/en/master/usage/restructuredtext/roles.html#role-term

.. note:: Tous les termes du glossaire se retrouvent dans la :ref:`page d'index <genindex>`
   et peuvent être mentionnés dans la documentation au moyen de la directive **:term:`<term>`**

   Exemple: On parle de :term:`template`

.. glossary::

    Algorithm
        A sequence of instructions that tell a biometric system how to
        solve a particular problem. An algorithm will have a finite number
        of steps and is typically used by the biometric engine (i.e. the
        biometric system software) to compute whether a biometric sample
        and template match.

    Biometric data
        Data encoding a feature or features used in biometric verification.

    Comparison
        The process of comparing a biometric sample with a previously
        stored reference template or templates.

    Comparison score
        Numerical value resulting from a comparison.

    Enrollment
        The process of collecting biometric samples from a person and the
        subsequent preparation and storage of biometric reference templates
        representing that person's identity.

    Extraction
        The process of converting a captured biometric sample into biometric
        data so that it can be compared to a reference template; sometimes
        called 'characterization'.

    False match
        Comparison decision of 'match' for a biometric probe and a biometric
        reference that are from different biometric capture subjects.

    False match rate
            Expected probability that a captured biometric sample will
            be falsely declared to match to a single, randomly-selected,
            non-self biometric reference.

    False non-match
        Comparison decision of 'non-match' for a biometric probe and a
        biometric reference that are from the same biometric capture subject
        and of the same biometric characteristic.

    False non-match rate
        Proportion of the completed biometric mated comparison trials that result in a false non-match.

    matching
    match
    Match / Matching
        The process of comparing a biometric sample against a previously stored template and scoring the level of similarity.

    One-to-many search
        Process in which a biometric probe of one biometric data subject is searched against the biometric references of more than one biometric data subject to return a candidate list or a comparison decision.

    One-to-one comparison
        Process in which a biometric probe from one biometric data subject is compared to a biometric reference from one biometric data subject to produce a comparison score.

    template
    Template / Reference template
        Data, which represents the biometric measurement of an enrollee, used by a biometric system for comparison against subsequently submitted biometric samples.

        .. note:: this term is not restricted to mean only data used in any particular recognition method, such as template matching.
