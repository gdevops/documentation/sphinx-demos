.. index::
   ! reSt
   ! reStructuredText

.. _input_rest:

=============================================================================
**reStructuredText (reSt)**
=============================================================================

- https://docutils.sourceforge.io/rst.html
- https://docutils.sourceforge.io/docs/user/rst/quickref.html

.. grid:: 1 2 3 4


    .. grid-item-card::
        :shadow: md
        :margin: 2 2 0 0


        .. card:: docutils
            :link: https://docutils.sourceforge.io/docs/user/rst/quickref.html



    .. grid-item-card::
        :shadow: md
        :margin: 2 2 0 0

        |sphinx|

        .. card:: Spĥinx official
            :link: https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html



    .. grid-item-card::
        :shadow: md
        :margin: 2 2 0 0

        .. card:: rest-sphinx-memo
            :link: https://rest-sphinx-memo.readthedocs.io/en/latest/ReST.html


    .. grid-item-card::
        :shadow: md
        :margin: 2 2 0 0

        .. card:: documatt
            :link: https://restructuredtext.documatt.com/
