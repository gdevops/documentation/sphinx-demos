.. index::
   pair: sphinx; configuration
   ! conf.py

.. _sphinx_conf:

======================================================================
**Configuration de ce projet sphinx**
======================================================================

.. literalinclude:: ../conf.py
   :linenos:
