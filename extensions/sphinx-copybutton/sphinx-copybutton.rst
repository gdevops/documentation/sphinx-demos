.. index::
   pair: sphinx-copybutton; extension
   pair: executablebooks; sphinx-copybutton
   ! sphinx-copybutton

.. _sphinx_copybutton:
.. _sphinx-copybutton:

======================================================================
**sphinx-copybutton** (Add a "copy" button to code blocks in Sphinx )
======================================================================

- https://github.com/executablebooks/sphinx-copybutton
- https://sphinx-copybutton.readthedocs.io/en/latest/index.html


Installation
=============


::

    poetry add sphinx-copybutton
    make update ou poetry export -f requirements.txt --without-hashes  > requirements.txt
