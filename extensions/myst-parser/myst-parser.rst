.. index::
   pair: sphinx; markdown
   pair: sphinx; myst-parser

.. _myst_parser:

========================================
|eb| |myst| **myst-parser**
========================================

- https://www.sphinx-doc.org/en/master/usage/markdown.html
- https://sphinx-extensions.readthedocs.io/en/latest/myst-parser.html
- https://myst-parser.readthedocs.io/en/latest/syntax/optional.html
- https://executablebooks.org/en/latest/tools/#myst-markedly-structured-text

Introduction
=================

Markdown is a lightweight markup language with a simplistic plain text
formatting syntax.

It exists in many syntactically different flavors.

**To support Markdown-based documentation, Sphinx can use MyST-Parser** |myst|

MyST-Parser is a Docutils bridge to markdown-it-py, a Python package for
parsing the CommonMark Markdown flavor.


Configuration
=====================

To configure your Sphinx project for Markdown support, proceed as follows

Install the Markdown parser MyST-Parser::

    pip install --upgrade myst-parser


or

    poetry add myst-parser

    make update ou poetry export -f requirements.txt --without-hashes  > requirements.txt



Add myst_parser to the list of configured extensions:

    extensions.append("myst_parser")


If you want to use Markdown files with extensions other than .md, adjust
the source_suffix variable.

The following example configures Sphinx to parse all files with the extensions
.md and .txt as Markdown::

    source_suffix = {
        '.rst': 'restructuredtext',
        '.txt': 'markdown',
        '.md': 'markdown',
    }


You can further configure MyST-Parser to allow custom syntax that standard
CommonMark doesn’t support.

Read more in the MyST-Parser documentation.



Convertisseur de .rst à markdown
==================================

- https://astrojuanlu.github.io/mystyc/
