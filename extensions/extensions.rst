.. index::
   pair: sphinx; extensions

.. _sphinx_extensions:

======================================================================
**Extensions sphinx**
======================================================================

- https://sphinx-extensions.readthedocs.io/en/latest/
- https://www.sphinx-doc.org/en/master/usage/extensions/index.html
- https://awesomesphinx.useblocks.com/
- https://www.sphinx-doc.org/en/master/usage/extensions/index.html#builtin-sphinx-extensions

.. toctree::
   :maxdepth: 3

   intersphinx/intersphinx
   myst-parser/myst-parser
   nb-sphinx/nb-sphinx
   sphinx-copybutton/sphinxcontrib-youtube
   sphinx-copybutton/sphinx-copybutton
   sphinx-subfigure/sphinx-subfigure
