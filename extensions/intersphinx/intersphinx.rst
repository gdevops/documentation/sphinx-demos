.. index::
   pair: sphinx  extension; intersphinx

.. _intersphinx:

================================================================================
Extension intégrée **intersphinx** (Link to other projects’ documentation)
================================================================================

- https://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html
- https://my-favorite-documentation-test.readthedocs.io/en/latest/using_intersphinx.html

Utilisation
============

Cette extension est intégrée à sphinx et pour l'utiliser il suffit
de le mentionner dans le tableau **extensions** du fichier **conf.py**.

.. code-block:: python

    extensions = [
        "sphinx.ext.autodoc",
        "sphinx.ext.doctest",
        "sphinx.ext.extlinks",
        "sphinx.ext.intersphinx",
        "sphinx.ext.todo",
        "sphinx.ext.viewcode",
        "sphinx_copybutton",
    ]

Description
=============

**L'extension 'sphinx.ext.intersphinx' est très puissante** car elle permet
d'établir des liens entre des documentations sphinx HTML distantes.

Pour établir ces connections, on ajoute dans le tableau **intersphinx_mapping**
les projets qui nous intéressent.

Extrait du fichier conf.py::

    intersphinx_mapping = {
        "intranet_user": (
            "https://informatique.gitlab.srv.int.id3.eu/intranet/intranet_user/",
            None,
        ),
        "infra_id3": ("http://informatique.gitlab.srv.int.id3.eu/Documentation/", None),
        "tuto_doc": ("https://gdevops.gitlab.io/documentation/tuto", None),
        "ray": ("https://docs.ray.io/en/latest/", None),
    }


Ensuite, il est possible d'établir des liens entre la documentation en cours
et ces documentations html en indiquant simplement les références définies
dans ces documents

.. important:: On n'a donc pas à définir des URLs en dur

Exemples de liens sur les 2 documentations sphinx ["tuto_doc", "ray"]
======================================================================================================

Lien sur la référence 'ref_rst_cheat_sheet' de la documentation **tuto_doc**
----------------------------------------------------------------------------------

Lien sur la :ref:`référence 'ref_rst_cheat_sheet' de la documentation 'tuto_formats'  <tuto_formats:ref_rst_cheat_sheet>`

Syntaxe reStructuredText employée::


    Lien sur la :ref:`référence 'ref_rst_cheat_sheet' de la documentation 'tuto_formats'  <tuto_formats:ref_rst_cheat_sheet>`

Lien sur la :ref:`référence 'tune-60-seconds' de la documentation 'ray'  <ray:tune-60-seconds>`
---------------------------------------------------------------------------------------------------

Lien sur la :ref:`référence 'tune-60-seconds' de la documentation 'ray'  <ray:tune-60-seconds>`

::

    Lien sur la :ref:`référence 'tune-60-seconds' de la documentation 'ray'  <ray:tune-60-seconds>`

Tips
=======

- https://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html#showing-all-links-of-an-intersphinx-mapping-file


.. tip:: Toutes les références d'une documentation peuvent être trouvées
   au moyen de la commande:

   python -m sphinx.ext.intersphinx <URL objects inv>

   Exemple::

       python -m sphinx.ext.intersphinx https://docs.ray.io/en/latest/objects.inv
