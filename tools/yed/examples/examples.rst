.. index::
   pair: download ; website_before.graphml.xml
   pair: xml ; website_before.graphml.xml

.. _yed_vrt_examples:

======================================================================
|yed| **network examples** |vrt|
======================================================================

- https://www.yworks.com/products/yed
- https://www.vrt.com.au/downloads/vrt-network-equipment


|arsouilles| Example https://www.arsouyes.org/blog/2023/2023-06-10_Simplifions/
===================================================================================

- https://www.arsouyes.org/blog/2023/2023-06-10_Simplifions/

website_before
---------------

.. figure:: images/website_before.png
   :align: center

   https://www.arsouyes.org/blog/2023/2023-06-10_Simplifions/website_before.png, https://www.arsouyes.org/blog/2023/2023-06-10_Simplifions/website_before.graphml


:download:`Télécharger le fichier website_before.graphml.xml <graphml/website_before.graphml.xml>`

whole_before
----------------

.. figure:: images/whole_before.png
   :align: center

   https://www.arsouyes.org/blog/2023/2023-06-10_Simplifions/whole_before.png, https://www.arsouyes.org/blog/2023/2023-06-10_Simplifions/whole_before.graphml


website_after
--------------------


.. figure:: images/whole_before.png
   :align: center

   https://www.arsouyes.org/blog/2023/2023-06-10_Simplifions/website_after.png, https://www.arsouyes.org/blog/2023/2023-06-10_Simplifions/website_after.graphml


network_current
-----------------

.. figure:: images/whole_before.png
   :align: center

   https://www.arsouyes.org/blog/2023/2023-06-10_Simplifions/network_current.png, https://www.arsouyes.org/blog/2023/2023-06-10_Simplifions/network_current.graphml


whole_after_compare
--------------------------

.. figure:: images/whole_before.png
   :align: center

   https://www.arsouyes.org/blog/2023/2023-06-10_Simplifions/whole_after_compare.png, https://www.arsouyes.org/blog/2023/2023-06-10_Simplifions/whole_after_compare.graphml
