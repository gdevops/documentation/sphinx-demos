.. index::
   pair: tool; yed
   ! vrt network extensions

.. _yed:

======================================================================
|yed| **yed + vrt network extensions** |vrt|
======================================================================

- https://www.yworks.com/products/yed
- https://www.vrt.com.au/downloads/vrt-network-equipment



|vrt| **Installation des graphiques vrt dans les palettes de yed**
==================================================================

- https://github.com/pafnow/vrt-graphml-for-yed

VRT GraphML palettes for yEd. Based on VRT extension for LibreOffice Draw.


- git clone https://github.com/pafnow/vrt-graphml-for-yed

    ::

        tree -L 2

    ::

        ├── LICENSE
        ├── README.md
        ├── VRT Clients and Peripherals.graphml
        ├── VRT Energy Management.graphml
        ├── VRT Industrial Automation.graphml
        ├── VRT Networking Communications.graphml
        ├── VRT Servers.graphml
        └── VRT Visualisation.graphml


- Open a .graphml-file
- Open Palette Manager
- Click New Section
- Choose Adopt from Document
- Choose an appropriate name

(thanks to Mitmisher)

- :download:`VRT Clients and Peripherals.graphml <vrt-graphml-for-yed/VRT Clients and Peripherals.graphml>`
- :download:`VRT Energy Management.graphml <vrt-graphml-for-yed/VRT Energy Management.graphml>`
- :download:`VRT Industrial Automation.graphml <vrt-graphml-for-yed/VRT Industrial Automation.graphml>`
- :download:`VRT Networking Communications.graphml <vrt-graphml-for-yed/VRT Networking Communications.graphml>`
- :download:`VRT Servers.graphml <vrt-graphml-for-yed/VRT Servers.graphml>`
- :download:`VRT Visualisation.graphml <vrt-graphml-for-yed/VRT Visualisation.graphml>`


5 nouvelles sections dans la palette de yED
---------------------------------------------

.. figure:: images/palette_VRT.png
   :align: center


Other formats
--------------------

- For Visio: https://github.com/pafnow/vrt-stencil-for-visio
- For Dia: https://github.com/cryptofuture/vrt-sheet-for-dia


Example arsouyes
==================

.. toctree::
   :maxdepth: 3

   examples/examples
