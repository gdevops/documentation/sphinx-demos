.. index::
   ! gitlab-ci


.. _gitlab_ci:

============================================================================
**Produire la documentation HTML grâce à l'intégration continue de gitlab**
============================================================================


Le fichier gitlab-ci.yml
==========================


.. literalinclude:: ../.gitlab-ci.yml
   :linenos:


Sélection du runner '**documentation**'
==========================================

- https://gitlab.srv.int.id3.eu/Informatique/intranet/demo-sphinx/-/settings/ci_cd

Pour produire la documentation, il faut un 'runner' gitlab qui
doit être sélectionné dans la `page gitlab 'settings/ci_cd' <https://gitlab.srv.int.id3.eu/Informatique/intranet/demo-sphinx/-/settings/ci_cd>`_ (https://gitlab.srv.int.id3.eu/Informatique/intranet/demo-sphinx/-/settings/ci_cd)

.. figure:: images/runner_documentation.png
   :align: center


URL de la documentation produite (https://informatique.gitlab.srv.int.id3.eu/intranet/demo-sphinx)
=====================================================================================================

- https://gitlab.srv.int.id3.eu/Informatique/intranet/demo-sphinx/pages

L'information donnant l'URL de la documentation produite se trouve dans `la page gitlab 'pages' <https://gitlab.srv.int.id3.eu/Informatique/intranet/demo-sphinx/pages>`_ (https://gitlab.srv.int.id3.eu/Informatique/intranet/demo-sphinx/pages)


.. figure:: images/access_pages.png
   :align: center

   https://informatique.gitlab.srv.int.id3.eu/intranet/demo-sphinx


Bonne pratique gitlab : **placer l'URL dans la description du projet gitlab**
===============================================================================

- https://gitlab.srv.int.id3.eu/Informatique/intranet/demo-sphinx/edit

Une fois connue l'URL de la documentation, on peut placer cette URL
dans la description du projet gitlab (https://gitlab.srv.int.id3.eu/Informatique/intranet/demo-sphinx/edit).


.. figure:: images/url_doc_dans_description_du_projet_gitlab.png
   :align: center


.. figure:: images/projet_avec_url.png
   :align: center

   https://gitlab.srv.int.id3.eu/Informatique/intranet/demo-sphinx
