

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/documentation/sphinx-demos/rss.xml>`_

.. _demo_sphinx_material:

=============================================================================
|sphinx| **Demo sphinx material**
=============================================================================


.. toctree::
   :maxdepth: 3

   sphinx-material/sphinx-material
   sphinx-design/sphinx-design
   conf/conf
   gitlab-ci/gitlab-ci
   extensions/extensions
   faq/faq
   input_format/input_format
   tools/tools
   glossaire/glossaire
