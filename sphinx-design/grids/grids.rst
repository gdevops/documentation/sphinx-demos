.. index::
   pair: sphinx-design; grids
   pair: sphinx-design; grilles
   ! grids

.. _sphinx_design_grids:

============
**grids**
============

- https://sphinx-design.readthedocs.io/en/furo-theme/grids.html
- https://getbootstrap.com/docs/5.0/layout/grid/

.. toctree::
   :glob:
   :hidden:

   *


Introduction
=================

Grids are based on a 12 column system, which can adapt to the size of
the viewing screen.

A grid directive can be set:

- with the number of default columns (1 to 12);
- either a single number for all screen sizes,
- or four numbers for extra-small (<576px), small (768px), medium (992px) and large screens (>1200px),

then child grid-item directives should be set for each item.


Placing a card in a grid
============================

The grid-item-card directive is a short-hand for placing a card content
container inside a grid item (see Cards). Most of the card directive’s
options can be used also here:


.. grid:: 2

    .. grid-item-card::  Title 1

        A

    .. grid-item-card::  Title 2

        B



Show some sphinx-extensions
===============================

- https://sphinx-extensions.readthedocs.io/en/latest/

.. How-To create the ``img-top``

   1. Capture a screenshot
   2. Crop to 300x
   3. Add a red arrow of 2px width
   4. Save it in PNG with 7 as compression level

.. grid:: 1 2 3 3


   .. grid-item-card:: sphinx-copybutton
      :img-top: images/sphinx-copybutton.png
      :link: sphinx-copybutton
      :link-type: doc
      :padding: 1

      Adds a little "Copy" button to the top-right of your code blocks.

   .. grid-item-card:: sphinx-favicon
      :img-top: images/sphinx-favicon.png
      :link: sphinx-favicon
      :link-type: doc
      :padding: 1

      Adds custom favicons to your Sphinx documentation.

   .. grid-item-card:: myst-parser
      :img-top: images/myst-parser.png
      :link: myst-parser
      :link-type: doc
      :padding: 1

      Powerful Markdown flavor for your Sphinx documentation without loosing the power of Sphinx itself.

   .. grid-item-card:: sphinx-autoapi
      :img-top: images/sphinx-autoapi.png
      :link: sphinx-autoapi
      :link-type: doc
      :padding: 1

      Auto documents your API code without executing the code itself (as ``sphinx.autodoc`` does).

   .. grid-item-card:: sphinx-notfound-page
      :img-top: images/sphinx-notfound-page.png
      :link: https://sphinx-notfound-page.readthedocs.io/
      :padding: 1

      Renders nice 404 pages respecting all the look & feel of your documentation.

   .. grid-item-card:: sphinx-version-warning
      :img-top: images/sphinx-version-warning.png
      :link: http://sphinx-version-warning.readthedocs.io/
      :padding: 1

      Adds a warning banner at the top if the reader is reading an old version of your documentation.

   .. grid-item-card:: sphinx-hoverxref
      :img-top: images/sphinx-hoverxref.png
      :link: https://sphinx-hoverxref.readthedocs.io/
      :padding: 1

      Adds tooltips on cross references of the documentation with the content of the linked section.

   .. grid-item-card:: sphinx-last-updated-by-git
      :img-top: images/sphinx-last-updated-by-git.png
      :link: https://github.com/mgeier/sphinx-last-updated-by-git
      :padding: 1

      Adds the "last updated" date at the bottom of each documentation page (obtained from the Git commit date).

   .. grid-item-card:: sphinxext-opengraph
      :img-top: images/sphinxext-opengraph.png
      :link: https://github.com/wpilibsuite/sphinxext-opengraph
      :padding: 1

      Generates Open Graph metadata ✨ for each page of your documentation.



grid link-type ref
----------------------

.. grid:: 1 2 3 3


   .. grid-item-card:: sphinx-copybutton
      :img-top: sphinx.png
      :link: tuto_sphinx:tuto_sphinx
      :link-type: ref
      :padding: 1

      Tuto sphinx

grid link-type ref
----------------------

.. card:: Tuto sphinx
    :link: tuto_sphinx:tuto_sphinx
    :link-type: ref

    Tuto sphinx.

Example from ray code use-cases.rst
==================================================

LLMs and Gen AI
---------------

Large language models (LLMs) and generative AI are rapidly changing industries,
and demand compute at an astonishing pace.

Ray provides a distributed compute framework for scaling these models,
allowing developers to train and deploy models faster and more efficiently.

With specialized libraries for data streaming, training, fine-tuning,
hyperparameter tuning, and serving, Ray simplifies the process of developing
and deploying large-scale AI models.

.. figure:: images/llm-stack.png

Learn more about how Ray scales LLMs and generative AI with the following resources.

.. grid:: 1 2 3 4
    :gutter: 1
    :class-container: container pb-3

    .. grid-item-card::
        :img-top: images/ray_logo.png
        :class-img-top: pt-2 w-75 d-block mx-auto fixed-height-img

        .. button-link:: https://www.anyscale.com/blog/ray-common-production-challenges-for-generative-ai-infrastructure

            [Blog] How Ray solves common production challenges for generative AI infrastructure

    .. grid-item-card::
        :img-top: images/ray_logo.png
        :class-img-top: pt-2 w-75 d-block mx-auto fixed-height-img

        .. button-link:: https://www.anyscale.com/blog/training-175b-parameter-language-models-at-1000-gpu-scale-with-alpa-and-ray

            [Blog] Training 175B Parameter Language Models at 1000 GPU scale with Alpa and Ray

    .. grid-item-card::
        :img-top: images/ray_logo.png
        :class-img-top: pt-2 w-75 d-block mx-auto fixed-height-img

        .. button-link:: https://www.anyscale.com/blog/faster-stable-diffusion-fine-tuning-with-ray-air

            [Blog] Faster stable diffusion fine-tuning with Ray AIR

    .. grid-item-card::
        :img-top: images/ray_logo.png
        :class-img-top: pt-2 w-75 d-block mx-auto fixed-height-img

        .. button-link:: https://www.anyscale.com/blog/how-to-fine-tune-and-serve-llms-simply-quickly-and-cost-effectively-using

            [Blog] How to fine tune and serve LLMs simply, quickly and cost effectively using Ray + DeepSpeed + HuggingFace

    .. grid-item-card::
        :img-top: images/ray_logo.png
        :class-img-top: pt-2 w-75 d-block mx-auto fixed-height-img

        .. button-link:: https://www.businessinsider.com/openai-chatgpt-trained-on-anyscale-ray-generative-lifelike-ai-models-2022-12

            [Blog] How OpenAI Uses Ray to Train Tools like ChatGPT
