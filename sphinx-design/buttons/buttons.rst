.. index::
   pair: sphinx-design; buttons
   ! buttons

.. _sphinx_design_buttons:

============
**buttons**
============

- https://sphinx-design.readthedocs.io/en/furo-theme/badges_buttons.html#buttons
- https://material.io/components/buttons

Introduction
=================

Buttons allow users to navigate to external (button-link) / internal (button-ref)
links with a single tap.


See the `Material Design <https://material.io/components/buttons>`_ and `Bootstrap descriptions <https://getbootstrap.com/docs/5.0/components/buttons/>`_
for further details.

button-link
=============

::

    .. button-link:: https://informatique.gitlab.srv.int.id3.eu/intranet/demo-sphinx/


.. button-link:: https://informatique.gitlab.srv.int.id3.eu/intranet/demo-sphinx/


button-link with text
=======================

::

    .. button-link:: https://informatique.gitlab.srv.int.id3.eu/intranet/demo-sphinx/

        Button text


.. button-link:: https://informatique.gitlab.srv.int.id3.eu/intranet/demo-sphinx/

    Button text


button-link with text, color and shadow
===========================================

::

    .. button-link:: https://informatique.gitlab.srv.int.id3.eu/intranet/demo-sphinx/
        :color: primary
        :shadow:


.. button-link:: https://informatique.gitlab.srv.int.id3.eu/intranet/demo-sphinx/
    :color: primary
    :shadow:

button-link with text, color and outline
===============================================

::

    .. button-link:: https://informatique.gitlab.srv.int.id3.eu/intranet/demo-sphinx/
        :color: primary
        :outline:

.. button-link:: https://informatique.gitlab.srv.int.id3.eu/intranet/demo-sphinx/
    :color: primary
    :outline:


button-link with text, color and expand
===============================================

::

    .. button-link:: https://informatique.gitlab.srv.int.id3.eu/intranet/demo-sphinx/
        :color: secondary
        :expand:

.. button-link:: https://informatique.gitlab.srv.int.id3.eu/intranet/demo-sphinx/
    :color: secondary
    :expand:
