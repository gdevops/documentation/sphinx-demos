.. index::
   pair: sphinx-design; badges

.. _sphinx_design_badges:

========
badges
========

- https://sphinx-design.readthedocs.io/en/latest/badges_buttons.html#badges
- https://getbootstrap.com/docs/5.0/components/badge/
- https://material.io/components/chip

bdg:plain badge
======================

::

    :bdg:`plain badge`



:bdg:`plain badge`


bdg-primary:primary
=========================================================

::

    :bdg-primary:`primary`  :bdg-primary-line:`primary-line`


:bdg-primary:`primary`  :bdg-primary-line:`primary-line`

bdg-secondary:secondary
============================

::

    :bdg-secondary:`secondary`  :bdg-secondary-line:`secondary-line`


:bdg-secondary:`secondary`  :bdg-secondary-line:`secondary-line`


bdg-success:success
==========================================================

::

    :bdg-success:`success`  :bdg-success-line:`success-line`

:bdg-success:`success`  :bdg-success-line:`success-line`

bdg-info:info
=================================================

::

    :bdg-info:`info`  :bdg-info-line:`info-line`


:bdg-info:`info` :bdg-info-line:`info-line`


bdg-warning:warning
==========================================================

::

    :bdg-warning:`warning`  :bdg-warning-line:`warning-line`


:bdg-warning:`warning` :bdg-warning-line:`warning-line`

bdg-danger:danger
======================================================

::

    :bdg-danger:`danger` :bdg-danger-line:`danger-line`


:bdg-danger:`danger` :bdg-danger-line:`danger-line`


:bdg-light:light
=====================================================

::

    :bdg-light:`light`  :bdg-light-line:`light-line`

:bdg-light:`light` :bdg-light-line:`light-line`


:bdg-dark:dark
======================

::

    :bdg-dark:`dark` :bdg-dark-line:`dark-line`

:bdg-dark:`dark` :bdg-dark-line:`dark-line`


bdg-link-primary
=====================

::

    :bdg-link-primary:`https://informatique.gitlab.srv.int.id3.eu/intranet/demo-sphinx/`

:bdg-link-primary:`https://informatique.gitlab.srv.int.id3.eu/intranet/demo-sphinx/`


bdg-link-primary-line
===========================

::

    :bdg-link-primary-line:`explicit title la demo sphinx material + sphinx-design <https://informatique.gitlab.srv.int.id3.eu/intranet/demo-sphinx/>`

:bdg-link-primary-line:`explicit title la demo sphinx material + sphinx-design <https://informatique.gitlab.srv.int.id3.eu/intranet/demo-sphinx/>`
