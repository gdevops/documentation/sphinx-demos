
.. _sphinx_design_installation:

===============
Installation
===============

::

    poetry add sphinx-design
    make update ou poetry export -f requirements.txt --without-hashes  > requirements.txt

et ajouter l'extension au tableau des extensions dans le fichier :ref:`conf.py <sphinx_conf>`

    extensions.append("sphinx_design")
