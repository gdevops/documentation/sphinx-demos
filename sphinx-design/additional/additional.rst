.. index::
   pair: sphinx-design; additional

.. _sphinx_design_additional:

======================================================================
additional
======================================================================

- https://sphinx-design.readthedocs.io/en/furo-theme/additional.html


Description
=============

These are additional components that are not part of the standard
Materials Design or Bootstrap systems.

article-info
=================

This directive is used to display a block of information about an article,
normally positioned just below the title of the article (shown below with
non-standard outline).

::

    .. article-info::
        :avatar: images/ebp-logo.png
        :avatar-link: https://executablebooks.org/
        :avatar-outline: muted
        :author: Executable Books
        :date: Jul 24, 2021
        :read-time: 5 min read
        :class-container: sd-p-2 sd-outline-muted sd-rounded-1



.. article-info::
    :avatar: images/ebp-logo.png
    :avatar-link: https://executablebooks.org/
    :avatar-outline: muted
    :author: Executable Books
    :date: Jul 24, 2021
    :read-time: 5 min read
    :class-container: sd-p-2 sd-outline-muted sd-rounded-1
