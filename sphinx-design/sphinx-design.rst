.. index::
   pair: sphinx-design; extension
   pair: sphinx-design; executablebooks

.. _sphinx_design:

========================================================================================================
|eb| L'extension **sphinx-design** (for designing beautiful, screen-size responsive web components)
========================================================================================================

- https://github.com/executablebooks/sphinx-design/tree/main
- https://sphinx-design.readthedocs.io/en/furo-theme/

.. toctree::
   :maxdepth: 3

   installation/installation
   tabs/tabs
   badges/badges
   icons/icons
   buttons/buttons
   cards/cards
   grids/grids
   dropdowns/dropdowns
   additional/additional
