.. index::
   pair: sphinx-design; cards


.. _cards:

================
**cards**
================

- https://sphinx-design.readthedocs.io/en/furo-theme/cards.html
- https://material.io/components/cards#anatomy

.. _cards-clickable:

Cards Clickable
==================

.. card:: Clickable Card (external)
    :link: https://gdevops.gitlab.io/tuto_documentation/formats/input/rest_sphinx/raw_html/raw_html.html#formats-input-rest-sphinx-raw-html-raw-html

    The entire card can be clicked to navigate to https://gdevops.gitlab.io/tuto_documentation/formats/input/rest_sphinx/raw_html/raw_html.html#formats-input-rest-sphinx-raw-html-raw-html


.. card:: Clickable Card (internal)
    :link: sphinx_design
    :link-type: ref

    The entire card can be clicked to navigate to the ``sphinx_design`` reference target.


.. grid:: 1 2 3 3


   .. grid-item-card:: sphinx-copybutton
      :img-top: sphinx.png
      :link: tuto_sphinx:tuto_sphinx
      :link-type: ref
      :padding: 1

      Tuto sphinx


.. card:: Tuto sphinx
    :link: tuto_sphinx:tuto_sphinx
    :link-type: ref

    Tuto sphinx.
