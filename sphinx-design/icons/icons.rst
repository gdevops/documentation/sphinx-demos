.. index::
   pair: sphinx-design; icons
   ! icons

.. _sphinx_design_icons:

============
**icons**
============

- https://sphinx-design.readthedocs.io/en/furo-theme/badges_buttons.html#inline-icons
- https://material.io/components/buttons


Description
=============

Inline icon roles are available for the GitHub octicon, Google Material Design Icons,
or FontAwesome libraries.

Octicon icons and Material icons are added as SVG’s directly into the page
with the octicon and material-<flavor> roles.

See below for the different flavors of Material Design Icons.

By default the icon will be of height 1em (i.e. the height of the font).

A specific height can be set after a semi-colon (;) with units of either
px, em or rem.

Additional CSS classes can also be added to the SVG after a second semi-colon (;) delimiter.

Octicon Icons
==============

For the full list see https://sphinx-design.readthedocs.io/en/furo-theme/badges_buttons.html#octicon-icons


report
---------

- :octicon:`report;1em;sd-text-info` report

file-directory
---------------

- :octicon:`file-directory;1em;sd-text-info` file-directory

broadcast
-----------

- :octicon:`broadcast;1em;sd-text-info` broadcast

eye
----

- :octicon:`eye;1em;sd-text-info` eye

pulse
--------

- :octicon:`pulse;1em;sd-text-info` pulse

rocket
-------

- :octicon:`rocket;1em;sd-text-info` rocket

For the full list see https://sphinx-design.readthedocs.io/en/furo-theme/badges_buttons.html#octicon-icons



Material Design Icons
==========================

- https://fonts.google.com/icons

Material Design icons come as several flavors. Each flavor represents a
different role used in sphinx-design.

These flavors are:

- material-regular
- material-outlined
- material-round
- material-sharp
- material-twotone

Not all icons are available for each flavor, but most are.

Instead of displaying the 10660+ icons here, you are encouraged to browse
the available icons from the `Material Design Icons’ <https://fonts.google.com/icons>`_ showcase hosted by Google.

::

    - A regular icon: :material-regular:`data_exploration;2em`, some more text
    - A coloured regular icon: :material-regular:`settings;3em;sd-text-success`, some more text.
    - A coloured outline icon: :material-outlined:`settings;3em;sd-text-success`, some more text.
    - A coloured sharp icon: :material-sharp:`settings;3em;sd-text-success`, some more text.
    - A coloured round icon: :material-round:`settings;3em;sd-text-success`, some more text.
    - A coloured two-tone icon: :material-twotone:`settings;3em;sd-text-success`, some more text.
    - A fixed size icon: :material-regular:`data_exploration;24px`, some more text.


- A regular icon: :material-regular:`data_exploration;2em`, some more text
- A coloured regular icon: :material-regular:`settings;3em;sd-text-success`, some more text.
- A coloured outline icon: :material-outlined:`settings;3em;sd-text-success`, some more text.
- A coloured sharp icon: :material-sharp:`settings;3em;sd-text-success`, some more text.
- A coloured round icon: :material-round:`settings;3em;sd-text-success`, some more text.
- A coloured two-tone icon: :material-twotone:`settings;3em;sd-text-success`, some more text.
- A fixed size icon: :material-regular:`data_exploration;24px`, some more text.
