.. index::
   pair: sphinx-design; tabs
   pair: sphinx-design; onglets
   pair: ray; tabs
   ! tabs
   ! onglets


.. design_tabs:

====================
**tabs** (Onglets)
====================

- https://sphinx-design.readthedocs.io/en/furo-theme/tabs.html
- https://sphinx-design.readthedocs.io/en/latest/tabs.html#tabbed-code-examples
- https://material.io/components/tabs

Introduction
================

**Tabs** organize and allow navigation between groups of content that are
related and at the same level of hierarchy.

Each tab should contain content that is distinct from other tabs in a set.


See the `Material Design <https://material.io/components/tabs>`_ description for further details.

.. _literalinclude_in_tab:


Example from ray code (1)
===========================

- https://docs.ray.io/en/latest/ray-core/actors.html#ray-remote-classes

::

    .. tab-set-code::

        .. literalinclude:: src/ray.py
           :language: python

        .. literalinclude:: src/ray.java
           :language: java

        .. literalinclude:: src/ray.c++
           :language: c++




.. tab-set-code::

    .. literalinclude:: src/ray.py
       :language: python

    .. literalinclude:: src/ray.java
       :language: java

    .. literalinclude:: src/ray.c++
       :language: c++



Example from ray code (2)
===========================

::

    .. tab-set::

        .. tab-item:: NumPy (default)

          The ``"numpy"`` option presents batches as ``Dict[str, np.ndarray]``, where the
          `numpy.ndarray <https://numpy.org/doc/stable/reference/generated/numpy.ndarray.html>`__
          values represent a batch of record field values.

          .. literalinclude:: ./doc_code/transforming_data.py
            :language: python
            :start-after: __writing_numpy_udfs_begin__
            :end-before: __writing_numpy_udfs_end__

        .. tab-item:: Pandas

          The ``"pandas"`` batch format presents batches in
          `pandas.DataFrame <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html>`__
          format.

          .. literalinclude:: ./doc_code/transforming_data.py
            :language: python
            :start-after: __writing_pandas_udfs_begin__
            :end-before: __writing_pandas_udfs_end__

        .. tab-item:: PyArrow

          The ``"pyarrow"`` batch format presents batches in
          `pyarrow.Table <https://arrow.apache.org/docs/python/generated/pyarrow.Table.html>`__
          format.

          .. literalinclude:: ./doc_code/transforming_data.py
            :language: python
            :start-after: __writing_arrow_udfs_begin__
            :end-before: __writing_arrow_udfs_end__



.. tab-set::

    .. tab-item:: NumPy (default)

      The ``"numpy"`` option presents batches as ``Dict[str, np.ndarray]``, where the
      `numpy.ndarray <https://numpy.org/doc/stable/reference/generated/numpy.ndarray.html>`__
      values represent a batch of record field values.

      .. literalinclude:: ./doc_code/transforming_data.py
        :language: python
        :start-after: __writing_numpy_udfs_begin__
        :end-before: __writing_numpy_udfs_end__

    .. tab-item:: Pandas

      The ``"pandas"`` batch format presents batches in
      `pandas.DataFrame <https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html>`__
      format.

      .. literalinclude:: ./doc_code/transforming_data.py
        :language: python
        :start-after: __writing_pandas_udfs_begin__
        :end-before: __writing_pandas_udfs_end__

    .. tab-item:: PyArrow

      The ``"pyarrow"`` batch format presents batches in
      `pyarrow.Table <https://arrow.apache.org/docs/python/generated/pyarrow.Table.html>`__
      format.

      .. literalinclude:: ./doc_code/transforming_data.py
        :language: python
        :start-after: __writing_arrow_udfs_begin__
        :end-before: __writing_arrow_udfs_end__



Synchronised Tabs
=====================

Use the sync option to synchronise the selected tab items across multiple
tab-sets.

Note, synchronisation requires that JavaScript is enabled.

.. tab-set::

    .. tab-item:: Label1
        :sync: key1

        Content 1

    .. tab-item:: Label2
        :sync: key2

        Content 2

.. tab-set::

    .. tab-item:: Label1
        :sync: key1

        Content 1

    .. tab-item:: Label2
        :sync: key2

        Content 2


Example from ray code ray-cluster-configuration
==================================================

``auth.ssh_private_key``
-----------------------------


.. tab-set::

    .. tab-item:: AWS
        :sync: key1

        Not available.

    .. tab-item:: Azure
        :sync: key2

        The path to an existing public key for Ray to use.

        * **Required:** Yes
        * **Importance:** High
        * **Type:** String

    .. tab-item:: GCP
        :sync: key3

        Not available.


``auth.ssh_public_key``
------------------------------

.. tab-set::

    .. tab-item:: AWS
        :sync: key1

        The cloud service provider. For AWS, this must be set to ``aws``.

        * **Required:** Yes
        * **Importance:** High
        * **Type:** String

    .. tab-item:: Azure
        :sync: key2

        The cloud service provider. For Azure, this must be set to ``azure``.

        * **Required:** Yes
        * **Importance:** High
        * **Type:** String

    .. tab-item:: GCP
        :sync: key3

        The cloud service provider. For GCP, this must be set to ``gcp``.

        * **Required:** Yes
        * **Importance:** High
        * **Type:** String

Minimal configuration
--------------------------

.. tab-set::

    .. tab-item:: AWS
        :sync: key1

        .. literalinclude:: autoscaler/aws/example-minimal.yaml
            :language: yaml

    .. tab-item:: Azure
        :sync: key2

        .. literalinclude:: autoscaler/azure/example-minimal.yaml
            :language: yaml

    .. tab-item:: GCP
        :sync: key3

        .. literalinclude:: autoscaler/gcp/example-minimal.yaml
            :language: yaml

TPU Configuration
----------------------

It is possible to use `TPU VMs <https://cloud.google.com/tpu/docs/users-guide-tpu-vm>`_
on GCP.

Currently, `TPU pods <https://cloud.google.com/tpu/docs/system-architecture-tpu-vm#pods>`_
(TPUs other than v2-8, v3-8 and v4-8) are not supported.

Before using a config with TPUs, ensure that the `TPU API is enabled for
your GCP project <https://cloud.google.com/tpu/docs/users-guide-tpu-vm#enable_the_cloud_tpu_api>`_.

.. tab-set::

    .. tab-item:: GCP
        :sync: key3

        .. literalinclude:: autoscaler/gcp/tpu.yaml
            :language: yaml
