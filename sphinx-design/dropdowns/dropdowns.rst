.. index::
   pair: sphinx-design; dropdowns
   pair: sphinx-design; champ déroulant
   pair: ray; dropdowns


.. _dropdowns:

==================================
**Dropdowns** (champ déroulant)
==================================

- https://sphinx-design.readthedocs.io/en/furo-theme/dropdowns.html

Description
=============

Dropdowns can be used to toggle, usually non-essential, content and show
it only when a user clicks on the header panel.

The dropdown can have a title, as the directive argument, and the open
option can be used to initialise the dropdown in the open state.


Example
=========

::

    .. dropdown::

        Dropdown content

    .. dropdown:: Dropdown title

        Dropdown content

    .. dropdown:: Open dropdown
        :open:

        Dropdown content


.. dropdown::

    Dropdown content

.. dropdown:: Dropdown title

    Dropdown content

.. dropdown:: Open dropdown
    :open:

    Dropdown content


Example from ray code (batch_inference.rst)
=================================================



.. note::

    You can use `ds.map_batches() <ray.data.Dataset.map_batches>` on functions, too.
    This is mostly useful for quick transformations of your data that doesn't require
    an ML model or other stateful objects.
    To handle state, using classes like we did above is the recommended way.
    In the dropdown below you find an example of mapping data with a simple Python
    function.

    .. dropdown:: Example using ``map_batches`` with functions

        This example transforms example data using a simple Python function.
        The ``map_function`` uses the fact that our ``data`` batches in this particular
        example are Pandas dataframes.
        Note that by using a map function instead of a class, we don't have to define
        `ActorPoolStrategy <ray.data.ActorPoolStrategy>` to specify compute resources.

        .. literalinclude:: ../tabs/doc_code/batch_formats.py
            :language: python
            :start-after: __simple_map_function_start__
            :end-before: __simple_map_function_end__


Example from ray code (trainers.rst)
=================================================


.. dropdown:: Code examples

    .. tab-set::

        .. tab-item:: Torch

            .. literalinclude:: doc_code/torch_trainer.py
                :language: python

        .. tab-item:: Tensorflow

            .. literalinclude:: doc_code/tf_starter.py
                :language: python
                :start-after: __air_tf_train_start__
                :end-before: __air_tf_train_end__

        .. tab-item:: Horovod

            .. literalinclude:: doc_code/hvd_trainer.py
                :language: python
