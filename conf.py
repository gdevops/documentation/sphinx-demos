# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config
# -- Path setup --------------------------------------------------------------
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
#
# Modeles de fichiers conf.py
# ============================
#
# - https://github.com/bashtage/sphinx-material/blob/master/docs/conf.py
# - https://github.com/bashtage/arch/blob/main/doc/source/conf.py
# - https://github.com/statsmodels/statsmodels/blob/main/docs/source/conf.py
#
# theme.conf
# ===========
# - https://github.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
#
import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

project = "Demo sphinx-material theme"
html_title = project
html_short_title = project
author = "id3 Technologies"
html_logo = "images/sphinx_avatar.png"
html_favicon = "images/sphinx_avatar.png"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
master_doc = "index"
version = "0.1.0"
release = version
today = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"
source_suffix = ".rst"
language = None
exclude_patterns = ["_build", ".venv", ".git"]

extensions = [
    "sphinx.ext.extlinks",
    "sphinx.ext.intersphinx",
    "sphinx.ext.todo",
    "sphinx.ext.viewcode",
    "sphinx_copybutton",
]

# https://www.sphinx-doc.org/en/master/usage/markdown.html
extensions.append("myst_parser")
# https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
myst_enable_extensions = [
    "amsmath",
    "attrs_inline",
    "colon_fence",
    "deflist",
    "dollarmath",
    "fieldlist",
    "html_admonition",
    "html_image",
    #    "linkify",
    "replacements",
    "smartquotes",
    "strikethrough",
    "substitution",
    "tasklist",
]
source_suffix = {
    ".rst": "restructuredtext",
    ".md": "markdown",
}

# https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
extensions.append("sphinx_design")

# https://sphinx-subfigure.readthedocs.io/en/latest/#usage
extensions.append("sphinx_subfigure")
numfig = True

# https://sphinxcontrib-youtube.readthedocs.io/en/latest/usage.html#configuration
extensions.append("sphinxcontrib.youtube")


# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

# __begin_intersphinx_mapping__
extensions.append("sphinx.ext.intersphinx")
intersphinx_mapping = {
    "tuto_doc": ("https://gdevops.frama.io/documentation/tuto", None),
    "doc_news": ("https://gdevops.frama.io/documentation/news", None),
    "tuto_sphinx": ("https://gdevops.frama.io/documentation/sphinx/tuto", None),
    "tuto_formats": ("https://gdevops.frama.io/documentation/formats", None),
    "tuto_python": ("https://gdevops.frama.io/python/tuto", None),
    "ray": ("https://docs.ray.io/en/latest/", None),
    "sphinx_doc": ("https://www.sphinx-doc.org/en/master/", None),
}
# python -m sphinx.ext.intersphinx http://informatique.gitlab.srv.int.id3.eu/Documentation/objects.inv
# python -m sphinx.ext.intersphinx https://informatique.gitlab.srv.int.id3.eu/intranet/intranet_user/objects.inv
# python -m sphinx.ext.intersphinx https://docs.ray.io/en/latest/objects.inv
# python -m sphinx.ext.intersphinx https://www.sphinx-doc.org/en/master/objects.inv
# __end_intersphinx_mapping__

extensions.append("sphinx.ext.todo")
todo_include_todos = True

# material theme options (see theme.conf for more information)
# https://github.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
# Colors
# The theme color for mobile browsers. Hex color.
# theme_color = #3f51b5
# Primary colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange,
# brown, grey, blue-grey, white
# Accent colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange
# color_accent = blue
# color_primary = blue-grey

html_theme_options = {
    "base_url": "https://gdevops.frama.io/documentation/sphinx-demos",
    "repo_url": "https://gitlab.com/gdevops/documentation/sphinx-demos",
    "repo_name": project,
    "repo_type": "gitlab",
    "html_minify": False,
    "html_prettify": False,
    "css_minify": False,
    "globaltoc_depth": -1,
    # https://www.colorhexa.com/2173f3
    "theme_color": "#a121f3",
    "color_primary": "indigo",
    "color_accent": "blue",
    "master_doc": False,
    "nav_title": f"{project} {release} ({today})",
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
        {
            "href": "https://gdevops.frama.io/documentation/linkertree",
            "internal": False,
            "title": "Liens documentation",
        },
    ],
    "heroes": {
        "index": "Demo sphinx-material theme",
    },
    "table_classes": ["plain"],
    # "use_sidenotes": True,  # L'option 'use_sidenotes' n'est pas supportée pour ce thème
}
# https://github.com/sphinx-contrib/yasfb
extensions.append("yasfb")
feed_base_url = html_theme_options["base_url"]
feed_author = "Scribe"
# https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
extensions.append("sphinx_design")
# https://sphinx-tags.readthedocs.io/en/latest/quickstart.html#installation
extensions.append("sphinx_tags")
tags_create_tags = True
# Whether to display tags using sphinx-design badges.
tags_create_badges = True


language = "fr"
html_last_updated_fmt = ""
todo_include_todos = True

html_use_index = True
html_domain_indices = True


def setup(app):
    # pour utiliser :confval:`html_show_sourcelink`
    app.add_object_type(
        "confval",
        "confval",
        objname="configuration value",
        indextemplate="pair: %s; configuration value",
    )


html_static_path = ["_static"]
html_css_files = ["custom.css"]


copyright = f"2023-{now.year}, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"

# en général les images avatar font 64x64
rst_prolog = """
.. |sphinx| image:: /images/sphinx_avatar.png
.. |myst| image:: /images/myst_avatar.png
.. |breathe| image:: /images/breathe_avatar.png
.. |doxygen| image:: /images/doxygen_avatar.png
.. |eb| image:: /images/executable_book_avatar.png
.. |vrt| image:: /images/vrt_avatar.png
.. |yed| image:: /images/yed_avatar.png
.. |arsouilles| image:: /images/arsouilles_avatar.png
.. |FluxWeb| image:: /images/rss_avatar.webp
"""
